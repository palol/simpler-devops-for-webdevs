# Simpler devops for webdevs

A basic containerised LAMP stack.

### 1. Dependencies for deploying on local environment
- Install Docker Community Edition: https://docs.docker.com/install/
- Install Docker Compose: https://docs.docker.com/compose/install/
- Install git git-cli: if you are on Windows https://git-scm.com/download/win if in nix use your package manager to install git and git-cli packages

### 2. Deployment on local environment
- Clone repository
- cd in cloned folder
- Run `docker-compose up`
- Webserver is reachable from http://localhost:8080

### 3. How to get started
- Install git: if you are on Windows https://git-scm.com/download/win, if in nix use your package manager to install git and git-cli packages.
- Get a GitLab account
https://gitlab.com/users/sign_in#register-pane
- Fork this repo to your own account
- Clone your repo on your local machine by typing `git clone https://gitlab.com/your-repo/simple-devops-for-web-developers.git` in the terminal/shell.
- If you want to move the cloned folder to another path in your system you always can.
- Enter into the cloned folder.
- Switch to branch master by typing `git checkout master`.
- Verify you enabled host firewall rules from and to the guest machine IP.
- Verify you installed dependencies
- To begin developing now switch back to branch develop by typing `git checkout develop` or name a new branch and switch to it.
- Submit your changes by doing a new merge request from your Gitlab account from a non master branch to my non master branch.
- Wish all contributors happy coding!

### 4. Community how-tos
- Send me an e-mail to be added to the developers team ( https://gitlab.com/palol/simple-devops-for-web-developers/-/project_members ).
- How to Collaborate On GitHub
https://code.tutsplus.com/tutorials/how-to-collaborate-on-github--net-34267
https://www.atlassian.com/git/tutorials/comparing-workflows
- Best practice in release management for open source projects http://oss-watch.ac.uk/resources/releasemanagementbestpractice

### 5. Best practices for this project
1. For developing don't use master branch.

### 6. Useful resources
- Git cheat sheet https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code
- What is a pull request http://oss-watch.ac.uk/resources/pullrequest

### 6. Credits
https://github.com/doctrine/doctrine2-orm-tutorial
